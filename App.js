import { useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import MainContainer from "./navigation/MainContainer";
import { fonts } from "./styles/fonts";
import AppLoading from "expo-app-loading";

export default function App() {
  const [font, setFont] = useState(false);

  if (font) {
    return <MainContainer />;
  } else {
    return (
      <AppLoading
        startAsync={fonts}
        onFinish={() => setFont(true)}
        onError={() => console.log("error")}
      />
    );
  }
}

const styles = StyleSheet.create({});
