import { StyleSheet, View } from "react-native";
import { TouchableOpacity, Text, Modal } from "react-native";
import IconFontAwesome from "react-native-vector-icons/FontAwesome";
import { useState } from "react";

export default function ListItem({
  item,
  note,
  deleteItem,
  setUpdateText,
  setView,
  setUpdateId,
}) {
  return (
    <TouchableOpacity style={styles.item}>
      <Text style={styles.text}>{item.title}</Text>

      {note ? (
        <View style={styles.buttons}>
          <TouchableOpacity onPress={() => deleteItem(item.id)}>
            <IconFontAwesome
              name={"trash-o"}
              color={"#787779"}
              size={35}
              style={{ marginBottom: 30, marginTop: 30 }}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              setView(true);
              setUpdateText(item.title);
              setUpdateId(item.id);
            }}
          >
            <IconFontAwesome name={"pencil"} color={"#787779"} size={30} />
          </TouchableOpacity>
        </View>
      ) : null}
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  item: {
    backgroundColor: "#FFFFFF",
    height: 150,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#787779",
    marginBottom: 40,
    marginLeft: 40,
    marginRight: 40,
    shadowColor: "#000000",
    shadowOffset: { width: 4, height: 4 },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 4,
    flex: 1,
    flexDirection: "row",
  },
  text: {
    fontWeight: "800",
    fontSize: 18,
    lineHeight: 16,
    padding: 20,
    fontFamily: "mt",
    flex: 2,
  },
  buttons: {
    marginRight: 23,
  },
});
