export const markers = [
  {
    id: 0,
    latitude: 55.76553275098326,
    longitude: 37.58196089845126,
    title: "Stand Up Club #1",
    description: "«Стендап Клуб #1» на Новом Арбате 21",
    pinColor: "green",
  },
  {
    id: 1,
    latitude: 55.77601314896662,
    longitude: 37.61068468668589,
    title: "StandUp Store Moscow",
    description:
      "StandUp Store Moscow – комедийный клуб Москвы, где каждый день проходят выступления самых известных комиков.",
    pinColor: "pink",
  },
];
