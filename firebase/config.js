import "@firebase/firestore";
import { getFirestore } from "@firebase/firestore";
import { initializeApp } from "firebase/app";
import "firebase/compat/firestore";
import firebase from "firebase/compat/app";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDBBMIYhpUMKLN_v-H-VCdl8z1i6AerRyA",
  authDomain: "notes-56f28.firebaseapp.com",
  projectId: "notes-56f28",
  storageBucket: "notes-56f28.appspot.com",
  messagingSenderId: "285041072588",
  appId: "1:285041072588:web:8064a2771bf03eb69ea569",
};

const app = firebase.initializeApp(firebaseConfig);

export const db = app.firestore();
