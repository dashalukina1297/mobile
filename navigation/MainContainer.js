import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { NavigationContainer } from "@react-navigation/native";
import * as React from "react";
import IconFontAwesome from "react-native-vector-icons/FontAwesome";
import Lists from "../screens/Lists";
import MapBlock from "../screens/MapBlock";
import Notes from "../screens/Notes";
import { LogBox, StyleSheet, View } from "react-native";

LogBox.ignoreLogs(["Setting a timer"]);
const Tab = createBottomTabNavigator();

export default function MainContainer() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={({ route }) => ({
          headerShown: false,
          tabBarShowLabel: false,
          tabBarStyle: { height: 100, backgroundColor: "#F9F9F9" },
          tabBarIcon: ({ focused }) => {
            let iconName;
            if (route.name === "Map") {
              iconName = "map-marker";
            } else if (route.name === "API") {
              iconName = "newspaper-o";
            } else if (route.name === "CRUD") {
              iconName = "pencil";
            }
            return (
              <View style={[styles.tab, focused ? styles.tabFocused : null]}>
                <IconFontAwesome name={iconName} color={"#787779"} size={35} />
              </View>
            );
          },
        })}
      >
        <Tab.Screen name='Map' component={MapBlock} />
        <Tab.Screen name='API' component={Lists} />
        <Tab.Screen name='CRUD' component={Notes} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}
const styles = StyleSheet.create({
  tab: {
    marginTop: 30,
    marginBottom: 30,
    shadowColor: "#000000",
    shadowOffset: { width: 4, height: 4 },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 4,
    borderRadius: 50,
    backgroundColor: "white",
    width: 80,
    height: 80,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  tabFocused: {
    borderColor: "#787779",
    borderWidth: 1,
  },
});
