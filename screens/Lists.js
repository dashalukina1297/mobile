import { StyleSheet } from "react-native";
import { useState } from "react";
import react from "react";
import { View, Text } from "react-native";
import ListItem from "../components/ListItem";
import { ScrollView } from "react-native";

export default function Lists({ navigation }) {
  const [items, setItems] = useState([]);

  react.useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/todos")
      .then((res) => res.json())
      .then((result) => setItems(result.slice(0, 20)));
  }, []);

  const listOfItems = items.map((value) => (
    <ListItem item={value} key={value.id} note={false} />
  ));
  return (
    <ScrollView style={{ marginTop: 50, backgroundColor: "white" }}>
      {listOfItems}
    </ScrollView>
  );
}

const styles = StyleSheet.create({});
