import MapView, { Marker } from "react-native-maps";
import { markers } from "../consts/markers";

export default function MapBlock() {
  return (
    <MapView
      style={{
        height: "50%",
        width: "100%",
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
      }}
      initialRegion={{
        latitude: 55.7522,
        longitude: 37.6156,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      }}
    >
      {markers.map((marker) => {
        return (
          <Marker
            coordinate={{
              latitude: Number(marker.latitude),
              longitude: Number(marker.longitude),
            }}
            title={marker.title}
            description={marker.description}
            pinColor={marker.pinColor}
            key={marker.id}
          />
        );
      })}
    </MapView>
  );
}
