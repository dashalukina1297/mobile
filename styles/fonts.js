import * as Font from "expo-font";

export const fonts = () => {
  return Font.loadAsync({
    mt: require("../assets/fonts/Montserrat-VariableFont_wght.ttf"),
  });
};
